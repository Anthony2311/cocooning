app.controller('AnnonceController', 
  ['$scope','AnnonceService','$state', '$ionicSlideBoxDelegate', 
  function($scope, AnnonceService, $state, $ionicSlideBoxDelegate) {

  AnnonceService.get($state.params.id)
    .then(function(result){
      $scope.annonce = result;
      
      $ionicSlideBoxDelegate.update();
    });

  $scope.next = function() {
    $ionicSlideBoxDelegate.next();
  };
  $scope.previous = function() {
    $ionicSlideBoxDelegate.previous();
  };

  // Called each time the slide changes
  $scope.slideChanged = function(index) {
    $scope.slideIndex = index;
  };

}]);