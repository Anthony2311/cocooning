app.controller('AnnoncesController', ['$scope','AnnonceService', function($scope, AnnonceService) {
  
  AnnonceService.getAll()
    .then(function(result){
        $scope.annonces = result;
    });

}]);