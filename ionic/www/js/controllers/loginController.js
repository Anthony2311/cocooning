app.controller('LoginController',['$scope','UserService', '$state', '$ionicHistory', function($scope, UserService, $state, $ionicHistory) {

    $scope.auth={

    };
    $scope.login = function(){
        console.log($scope.auth.email)
        console.log($scope.auth.mdp);
        UserService.login($scope.auth.email, $scope.auth.mdp)
            .then(function(result){
                $ionicHistory.nextViewOptions({
                    disableBack: true
                });
                $state.go('app.annonce');
            },
            function(result){
                $scope.error="Login invalide"
            });
    };
    

}]);