app.controller('MainController', ['$scope','UserService', '$state', function($scope, UserService, $state) {

  $scope.isLogin = UserService.isLogin;
  
  $scope.logout = function(){
    UserService.logout();
    $state.go("app.login");
  }
}]);
