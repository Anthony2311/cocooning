app.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

  .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'views/menu.html',
    controller: 'MainController'
  })

  .state('app.login', {
    url: '/login',
    views: {
      'menuContent': {
        templateUrl: 'views/login.html',
        controller: 'LoginController'
      }
    }
  })

  .state('app.inscription', {
      url: '/inscription',
      views: {
        'menuContent': {
          templateUrl: 'views/inscription.html',
          controller: 'InscriptionController'
        }
      }
    })
    .state('app.annonce', {
      url: '/annonce',
      views: {
        'menuContent': {
          templateUrl: 'views/annonces.html',
          controller: 'AnnoncesController'
        }
      }
    })

  .state('app.annonceDetail', {
    url: '/annonce/:id',
    views: {
      'menuContent': {
        templateUrl: 'views/annonce.html',
        controller: 'AnnonceController'
      }
    }
  })

    .state('app.management', {
      url: '/management',
      views: {
        'menuContent': {
          templateUrl: 'views/management.html',
          controller: 'ManagementController'
        }
      }
    })

    .state('app.management.monCompte', {
    url: '/monCompte',
    views: {
      'management-tab': {
        templateUrl: 'views/monCompte.html',
        controller: 'MonCompteController'
      }
    }
  });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/annonce');
});
