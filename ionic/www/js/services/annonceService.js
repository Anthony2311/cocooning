app.service('AnnonceService', function($q, $http) {

    var url = "http://localhost:59566/";

    this.getAll = function(){
        
        var defer = $q.defer();
        
        $http.get(url+'api/annonces')
            .then(function(result){
                for(i=0; i<result.data.length; i++){
                    result.data[i].photo=["img/img"+Math.floor((Math.random() * 10) + 1)+".jpg",
                                          "img/img"+Math.floor((Math.random() * 10) + 1)+".jpg"];
                }
                defer.resolve(result.data);
            },
            function(error){
                defer.reject(error);
            });
        
        return defer.promise;
    }

    this.get = function(id){
        console.log(id);
        var defer = $q.defer();
        
        $http.get(url+'api/annonces/'+id)
            .then(function(result){
                result.data.photo=["img/img"+Math.floor((Math.random() * 10) + 1)+".jpg",
                                          "img/img"+Math.floor((Math.random() * 10) + 1)+".jpg"];
                defer.resolve(result.data);
            },
            function(error){
                defer.reject(error);
            });

        return defer.promise;
        
    }

    this.create = function(annonce){
        var defer = $q.defer();
        
        $http.post(url+'api/annonces', annonce)
            .then(function(result){
                defer.resolve(result.data);
            },
            function(error){
                defer.reject(error);
            });

        return defer.promise;
    }

    this.update = function(annonce){
        var defer = $q.defer();
        
        $http.put(url+'api/annonces'+annonce.id, annonce)
            .then(function(result){
                defer.resolve(result.data);
            },
            function(error){
                defer.reject(error);
            });

        return defer.promise;
    }

});
