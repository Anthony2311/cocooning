app.service('UserService', function($q, $http) {

    var url = "http://localhost:59566/";
    var userProfile = JSON.parse(localStorage.getItem('profile')) || null;

    this.isLogin = function(){
        return userProfile != null;
    }

    this.login = function(email, mdp){
        var defer = $q.defer();

            $http.get(url+'api/users/login?email='+email+'&password='+mdp)
                .then(function(result){
                    
                var user = result.data.user;
                var token = result.data.token;

                localStorage.setItem('token', token);
                localStorage.setItem('profile', JSON.stringify(user));
                userProfile = user;

                defer.resolve(user);
            },
            function(error){
                defer.reject(error);
            }); 

        return defer.promise;
    }

    this.logout = function(){
        localStorage.removeItem('token');
        localStorage.removeItem('profile');
        userProfile = null;
    }

    this.create = function(user){
        var defer = $q.defer();
        
        defer.resolve(user);

        return defer.promise;
    }

    this.update = function(user){
        var defer = $q.defer();
        
        defer.resolve(user);

        return defer.promise;
    }

});
