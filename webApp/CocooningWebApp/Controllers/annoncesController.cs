﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using CocooningWebApp.Models;
using System.Threading.Tasks;

namespace CocooningWebApp.Controllers
{
    [AllowAnonymous]
    public class annoncesController : ApiController
    {
        private Cocooning db = new Cocooning();

        // GET: api/annonces
        [AllowAnonymous]
        public async Task<List<annonces>> Getannonces()
        {
            try
            {
                var annonces = await db.annonces.ToListAsync();
                return annonces;
            }
            catch (Exception e)
            {

            }
            return null; 
            
        }

        // GET: api/annonces/5
        [AllowAnonymous]
        [ResponseType(typeof(annonces))]
        public IHttpActionResult Getannonce(int id)
        {
            annonces annonce = db.annonces.Find(id);
            if (annonce == null)
            {
                return NotFound();
            }

            return Ok(annonce);
        }

        // PUT: api/annonces/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Putannonce(int id, annonces annonce)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != annonce.idannonces)
            {
                return BadRequest();
            }

            db.Entry(annonce).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!annonceExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/annonces
        [ResponseType(typeof(annonces))]
        public IHttpActionResult Postannonce(annonces annonce)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.annonces.Add(annonce);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = annonce.idannonces }, annonce);
        }

        // DELETE: api/annonces/5
        [ResponseType(typeof(annonces))]
        public IHttpActionResult Deleteannonce(int id)
        {
            annonces annonce = db.annonces.Find(id);
            if (annonce == null)
            {
                return NotFound();
            }

            db.annonces.Remove(annonce);
            db.SaveChanges();

            return Ok(annonce);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool annonceExists(int id)
        {
            return db.annonces.Count(e => e.idannonces == id) > 0;
        }
    }
}