﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using CocooningWebApp.Models;
using System.Threading.Tasks;

namespace CocooningWebApp.Controllers
{

    [Authorize]
    [AllowAnonymous]
    [RoutePrefix("api/users")]
    public class usersController : ApiController
    {
        private Cocooning db = new Cocooning();

        // GET: api/users
        [AllowAnonymous]
        [Route("login")]
        public async Task<IHttpActionResult> Getlogin(string email, string password)
        {
            try
            {
                //var user = await db.users.Where(u => u.email == email && u.mdp == password).FirstOrDefaultAsync();

                //if (user == null)
                //{
                //    return NotFound();
                //}

                var loginCredentials = new LoginCredentials()
                {
                    //user = user,
                    user = new users(),
                    token = ""
                };

                return Ok(loginCredentials);
            }
            catch(Exception e)
            {
                return null;
            }
            
        }

        // GET: api/users
        public IQueryable<users> Getusers()
        {
            return db.users;
        }

        // GET: api/users/5
        [ResponseType(typeof(users))]
        public IHttpActionResult Getuser(int id)
        {
            users user = db.users.Find(id);
            if (user == null)
            {
                return NotFound();
            }

            return Ok(user);
        }

        // PUT: api/users/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Putuser(int id, users user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != user.idUsers)
            {
                return BadRequest();
            }

            db.Entry(user).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!userExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/users
        [ResponseType(typeof(users))]
        public IHttpActionResult Postuser(users user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.users.Add(user);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = user.idUsers }, user);
        }

        // DELETE: api/users/5
        [ResponseType(typeof(users))]
        public IHttpActionResult Deleteuser(int id)
        {
            users user = db.users.Find(id);
            if (user == null)
            {
                return NotFound();
            }

            db.users.Remove(user);
            db.SaveChanges();

            return Ok(user);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool userExists(int id)
        {
            return db.users.Count(e => e.idUsers == id) > 0;
        }
    }
}