namespace CocooningWebApp.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Cocooning : DbContext
    {
        public Cocooning()
            : base("name=Cocooning")
        {
        }

        public virtual DbSet<adresse> adresse { get; set; }
        public virtual DbSet<annonces> annonces { get; set; }
        public virtual DbSet<commentaire> commentaire { get; set; }
        public virtual DbSet<documents> documents { get; set; }
        public virtual DbSet<includ> includ { get; set; }
        public virtual DbSet<photo> photo { get; set; }
        public virtual DbSet<reservation> reservation { get; set; }
        public virtual DbSet<service> service { get; set; }
        public virtual DbSet<users> users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<adresse>()
                .Property(e => e.ville)
                .IsUnicode(false);

            modelBuilder.Entity<adresse>()
                .Property(e => e.code_postal)
                .IsUnicode(false);

            modelBuilder.Entity<adresse>()
                .Property(e => e.pays)
                .IsUnicode(false);

            modelBuilder.Entity<adresse>()
                .HasMany(e => e.users)
                .WithOptional(e => e.adresse)
                .HasForeignKey(e => e.Adresse_idAdresse);

            modelBuilder.Entity<annonces>()
                .Property(e => e.adresse)
                .IsUnicode(false);

            modelBuilder.Entity<annonces>()
                .Property(e => e.description)
                .IsUnicode(false);

            modelBuilder.Entity<annonces>()
                .Property(e => e.prix)
                .IsUnicode(false);

            modelBuilder.Entity<annonces>()
                .Property(e => e.visible)
                .IsUnicode(false);

            modelBuilder.Entity<annonces>()
                .Property(e => e.titre)
                .IsUnicode(false);

            //modelBuilder.Entity<annonces>()
            //    .HasMany(e => e.includ)
            //    .WithOptional(e => e.annonces)
            //    .HasForeignKey(e => e.Annonces_idannonces);

            //modelBuilder.Entity<annonces>()
            //    .HasMany(e => e.photo)
            //    .WithOptional(e => e.annonces)
            //    .HasForeignKey(e => e.Annonces_idannonces);

            //modelBuilder.Entity<annonces>()
            //    .HasMany(e => e.reservation)
            //    .WithOptional(e => e.annonces)
            //    .HasForeignKey(e => e.Annonces_idannonces);

            modelBuilder.Entity<commentaire>()
                .Property(e => e.contenu)
                .IsUnicode(false);

            modelBuilder.Entity<documents>()
                .Property(e => e.type)
                .IsUnicode(false);

            modelBuilder.Entity<documents>()
                .Property(e => e.chemin)
                .IsUnicode(false);

            modelBuilder.Entity<photo>()
                .Property(e => e.chemin)
                .IsUnicode(false);

            modelBuilder.Entity<photo>()
                .Property(e => e.pcouv)
                .IsUnicode(false);

            modelBuilder.Entity<reservation>()
                .Property(e => e.statut)
                .IsUnicode(false);

            modelBuilder.Entity<reservation>()
                .Property(e => e.payer)
                .IsUnicode(false);

            modelBuilder.Entity<reservation>()
                .HasMany(e => e.documents)
                .WithOptional(e => e.reservation)
                .HasForeignKey(e => e.Reservation_idReservation);

            modelBuilder.Entity<service>()
                .Property(e => e.nom)
                .IsUnicode(false);

            modelBuilder.Entity<service>()
                .HasMany(e => e.includ)
                .WithOptional(e => e.service)
                .HasForeignKey(e => e.Service_idService);

            modelBuilder.Entity<users>()
                .Property(e => e.nom)
                .IsUnicode(false);

            modelBuilder.Entity<users>()
                .Property(e => e.Prenom)
                .IsUnicode(false);

            modelBuilder.Entity<users>()
                .Property(e => e.mdp)
                .IsUnicode(false);

            modelBuilder.Entity<users>()
                .Property(e => e.rue)
                .IsUnicode(false);

            modelBuilder.Entity<users>()
                .Property(e => e.numero)
                .IsUnicode(false);

            modelBuilder.Entity<users>()
                .Property(e => e.email)
                .IsUnicode(false);

            modelBuilder.Entity<users>()
                .HasMany(e => e.commentaire)
                .WithOptional(e => e.users)
                .HasForeignKey(e => e.commenteur);

            modelBuilder.Entity<users>()
                .HasMany(e => e.commentaire1)
                .WithOptional(e => e.users1)
                .HasForeignKey(e => e.proprietaire);

            modelBuilder.Entity<users>()
                .HasMany(e => e.reservation)
                .WithOptional(e => e.users)
                .HasForeignKey(e => e.Users_idUsers);
        }
    }
}
