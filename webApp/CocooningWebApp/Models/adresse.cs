namespace CocooningWebApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("cocooningstudents.adresse")]
    public partial class adresse
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public adresse()
        {
            users = new HashSet<users>();
        }

        [Key]
        public int idAdresse { get; set; }

        [StringLength(45)]
        public string ville { get; set; }

        [Column("code postal")]
        [StringLength(10)]
        public string code_postal { get; set; }

        [StringLength(45)]
        public string pays { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<users> users { get; set; }
    }
}
