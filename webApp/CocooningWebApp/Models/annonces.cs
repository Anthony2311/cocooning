namespace CocooningWebApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("cocooningstudents.annonces")]
    public partial class annonces
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public annonces()
        {
            //includ = new HashSet<includ>();
            //photo = new HashSet<photo>();
            //reservation = new HashSet<reservation>();
        }

        [Key]
        public int idannonces { get; set; }

        [StringLength(45)]
        public string adresse { get; set; }

        [StringLength(255)]
        public string description { get; set; }

        [StringLength(45)]
        public string prix { get; set; }

        public DateTime? date { get; set; }

        [StringLength(45)]
        public string visible { get; set; }

        [StringLength(45)]
        public string titre { get; set; }

        //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        //public virtual ICollection<includ> includ { get; set; }

        //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        //public virtual ICollection<photo> photo { get; set; }

        //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        //public virtual ICollection<reservation> reservation { get; set; }
    }
}
