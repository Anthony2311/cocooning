namespace CocooningWebApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("cocooningstudents.commentaire")]
    public partial class commentaire
    {
        [Key]
        public int idcommentaire { get; set; }

        [StringLength(45)]
        public string contenu { get; set; }

        public int? commenteur { get; set; }

        public int? proprietaire { get; set; }

        public virtual users users { get; set; }

        public virtual users users1 { get; set; }
    }
}
