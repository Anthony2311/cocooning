namespace CocooningWebApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("cocooningstudents.documents")]
    public partial class documents
    {
        [Key]
        public int idDocuments { get; set; }

        [StringLength(45)]
        public string type { get; set; }

        [StringLength(45)]
        public string chemin { get; set; }

        public int? Reservation_idReservation { get; set; }

        public virtual reservation reservation { get; set; }
    }
}
