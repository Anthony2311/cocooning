namespace CocooningWebApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("cocooningstudents.includ")]
    public partial class includ
    {
        public int? Service_idService { get; set; }

        public int? Annonces_idannonces { get; set; }

        [Key]
        public int idinclud { get; set; }

        public virtual annonces annonces { get; set; }

        public virtual service service { get; set; }
    }
}
