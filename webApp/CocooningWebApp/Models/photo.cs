namespace CocooningWebApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("cocooningstudents.photo")]
    public partial class photo
    {
        [Key]
        public int idPhoto { get; set; }

        [StringLength(45)]
        public string chemin { get; set; }

        [StringLength(45)]
        public string pcouv { get; set; }

        public int? Annonces_idannonces { get; set; }

        public virtual annonces annonces { get; set; }
    }
}
