namespace CocooningWebApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("cocooningstudents.reservation")]
    public partial class reservation
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public reservation()
        {
            documents = new HashSet<documents>();
        }

        [Key]
        public int idReservation { get; set; }

        public DateTime? debut { get; set; }

        [StringLength(45)]
        public string statut { get; set; }

        [StringLength(45)]
        public string payer { get; set; }

        public int? Annonces_idannonces { get; set; }

        public int? Users_idUsers { get; set; }

        public virtual annonces annonces { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<documents> documents { get; set; }

        public virtual users users { get; set; }
    }
}
