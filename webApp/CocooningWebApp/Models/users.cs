namespace CocooningWebApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("cocooningstudents.users")]
    public partial class users
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public users()
        {
            commentaire = new HashSet<commentaire>();
            commentaire1 = new HashSet<commentaire>();
            reservation = new HashSet<reservation>();
        }

        [Key]
        public int idUsers { get; set; }

        [StringLength(45)]
        public string nom { get; set; }

        [StringLength(45)]
        public string Prenom { get; set; }

        [StringLength(45)]
        public string mdp { get; set; }

        [StringLength(45)]
        public string rue { get; set; }

        [StringLength(45)]
        public string numero { get; set; }

        public int? Adresse_idAdresse { get; set; }

        [StringLength(45)]
        public string email { get; set; }

        public virtual adresse adresse { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<commentaire> commentaire { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<commentaire> commentaire1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<reservation> reservation { get; set; }
    }

    public class LoginCredentials
    {
        public users user;
        public string token;
    }
}
