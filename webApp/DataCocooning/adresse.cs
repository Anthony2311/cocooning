//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataCocooning
{
    using System;
    using System.Collections.Generic;
    
    public partial class adresse
    {
        public adresse()
        {
            this.users = new HashSet<users>();
        }
    
        public int idAdresse { get; set; }
        public string ville { get; set; }
        public string code_postal { get; set; }
        public string pays { get; set; }
    
        public virtual ICollection<users> users { get; set; }
    }
}
